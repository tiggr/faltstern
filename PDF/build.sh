#!/bin/bash

out="handout.pdf"

while [ "$1" != '' ]
  do
    [ $1 == "-nolist" ] && nolist=true && shift
    [ $1 == "-o" ] && shift && out=$1 && shift
done

if [ "$nolist" == true ]
  then
    list="T"
    listpdf="leer.pdf"
    listpdftk="${list}=${listpdf}"
  else
    list="T"
    listpdf="../../Teilnehmer/Teilnehmer.pdf"
    listpdftk="${list}=${listpdf}"
fi

echo cleaning before build ...
rm handout.pdf 2> /dev/null
rm temp* 2> /dev/null

# rearranging pages
#pdftk ../Florian/Faltanleitungen.pdf cat 5 2 1 4 3 output tempfalten.pdf

echo building handout ...
# pdftk C=../main.pdf $listpdftk E=leer.pdf O=oss.pdf cat C ${list} E O output temp.pdf verbose
pdftk C=../main.pdf E=leer.pdf P=../Florian/Faltanleitungen.pdf O=../backside.pdf $listpdftk cat C P E O output temp.pdf verbose
# pdftk C=../main.pdf E=leer.pdf P=../Florian/Faltanleitungen.pdf O=../backside.pdf $listpdftk cat C P $list E E O output temp.pdf verbose
# pdftk C=../main.pdf E=leer.pdf P=../Florian/Faltanleitungen.pdf O=../backside.pdf $listpdftk cat C E P E $list O output temp.pdf verbose

echo updating metadata and compress
pdftk temp.pdf update_info meta.txt output $out compress

echo cleaning after build ...
rm temp*.* 2> /dev/null

echo Done!
